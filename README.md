Basic implementation that allow some options for generating a queue of targets to observe.    
The main optimisation criteria is to minimise slew time between targets, but this is conditioned to
the targets being visible at the time of observation.    
The algorithm implements the Nearest Neighbour algorithm to find the shortest travel path between
targets, derived from the standard travelling salesman problem.


## Development installation
```
git clone git@bitbucket.org:r_et_d/slewtime.git
cd slewtime
pip install -e .
```

## Minimize slew distance (degrees total) for a given group of observation targets
**Very important**: Basic assumption is that each node is visited once and that sequence does not matter
and no targets are paired in target/calibrator sets.

To evaluate a full Meertime observation, simply set the date of observation    
`
pulsar_observation.py -f testdata/meertime_msp_regular_nov19.csv -c testdata/psrs.csv --date '2019-11-19
00:00:00'
`    
`
pulsar_observation.py -f testdata/meertime_tpa_new4.csv -c testdata/psrs.csv --date '2019-11-19
00:00:00'
`    
Observation time and grouping is made easier with the graphic display of the observation.

Also see [wiki](https://bitbucket.org/r_et_d/slewtime/wiki/Home)

## Citation
When using this little tool, please add the citation to the larger research body from which is was
extracted:   
```
Ruby van Rooyen, Deneys S. Maartens, and Peter Martinez "Autonomous observation scheduling in
astronomy", Proc. SPIE 10704, Observatory Operations: Strategies, Processes, and Systems VII, 1070410
(10 July 2018); https://doi.org/10.1117/12.2311839
```
