from datetime import datetime
from slewtime import plot_queue
from slewtime import utils
from slewtime.observatory import Observatory
from slewtime.queue import Queue

import argparse
import matplotlib.pyplot as plt


# user input options
def cli_():
    usage = "%(prog)s [options]"
    description = 'optimal target observation order to minimise slew time'
    parser = argparse.ArgumentParser(
        usage=usage,
        description=description,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--all',
        action='store_true',
        help='show all observation sequence permutations')
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='verbose output')
    parser.add_argument(
        '--debug',
        action='store_true',
        help='very verbose debug output')
    parser.add_argument(
        '--trace',
        action='store_true',
        help='very very verbose output for tracing bugs')

    group = parser.add_argument_group(
        title="setup script targets",
        description="arguments to get example input")
    group.add_argument(
        '-f', '--meertime',
        type=str,
        required=True,
        help='meertime target CSV file: Name, duration, bool, DM')
    group.add_argument(
        '-c', '--catalogue',
        type=str,
        required=True,
        help='pulsar catalogue: Name, tag, RA, Dec')
    group.add_argument(
        '-n', '--number',
        type=int,
        help='number of targets to observe')
    group.add_argument(
        '-s', '--seed',
        type=int,
        help='seed for pseudo-random sort')

    group = parser.add_argument_group(
        title="optimization algorithm arguments",
        description="target slew optimization assuming the MeerKAT telescope")
    group.add_argument(
        '--datetime',
        type=str,
        help='start time for observation (UTC) using format '
             'date and time of observation ("YYYY-MM-DD HH:mm:SS")')
    group.add_argument(
        '--horizon',
        type=float,
        default=20.,
        help='minimum horizon angle [degrees]')

    args = parser.parse_args()

    # check datetime input format before continuing
    if args.datetime is not None:
        datetime.strptime(args.datetime, '%Y-%m-%d %H:%M:%S')

    if args.seed is not None:
        # accommodate humans that count from 1
        if args.seed > 0:
            args.seed -= 1

    # set verbosity level as a count
    # 0 = not verbose output
    # 1 = verbose output
    # 2 = debug level output
    # 3 = trace level output
    if args.trace:
        args.debug = True
    if args.debug:
        args.verbose = True
    args.verbosity = args.verbose + args.debug + args.trace

    return args


# utility function to randomly select N targets
class Targets(object):
    def __init__(self,
                 meertime_file,
                 catalogue):
        meertime_targets = self.read_meertime(meertime_file)
        [headers, catalogue_pulsars] = self.read_csv(catalogue)
        self.target_list = self.construct_list(meertime_targets,
                                               catalogue_pulsars)

    def read_meertime(self, filename):
        """Assume input file format
           first column is target name,
           second column is duration,
           [third column is a boolean and fourth the pulsar DM]
        """
        target_list = []
        with open(filename, 'r') as fin:
            targets = fin.readlines()
        for line in targets:
            if len(line.strip()) < 1:
                continue  # skip empty line
            [target, duration, _, _] = line.strip().split(',')
            target_list.append([target, duration])
        # return target list containing [name, duration]
        return target_list

    def read_csv(self, filename):
        """Returns targets as a list of strings"""
        with open(filename, 'r') as fin:
            catalogue = fin.readlines()
        headers = ''
        target_list = []
        for line in catalogue:
            if '#' in line:
                headers += line
                continue  # ignore comments
            if len(line.strip()) < 1:
                continue  # ignore empty rows
            target_list.append(line.strip())
        # assuming format: name, tags, ra, decl
        return headers, target_list

    def __index__(self, list_, substr):
        for i, s in enumerate(list_):
            if substr in s:
                return i
        return -1

    def construct_list(self, targets, catalogue):
        target_list = []
        for target, duration in targets:
            idx = self.__index__(catalogue, target)
            if idx < 0:
                print('Unknown pulsar {}'.format(target))
                continue
            [_, _, ra, decl] = catalogue[idx].split(',')
            target_str = "{}, {}, {}, {}, optional extras".format(
                target,
                ra.strip(),
                decl.strip(),
                duration)
            target_list.append(target_str)
        return target_list


# utility function to mimic input data
def input_(meertime_cat,  # example Meertime observation
           pulsar_cat,  # pulsar catalogue
           number=10,  # number of targets in list
           seed=None,  # seed random selection
           verbosity=0,
           ):
    # target input setup to mimic interface
    target_list = Targets(meertime_cat,
                          pulsar_cat).target_list
    if verbosity > 1:
        print('\nexample input')
        for tgt in target_list:
            print(tgt)

    # list of random targets for planning development
    if (number is not None and seed is not None):
        number += seed
    if number is None:
        number = len(target_list)
    target_list = utils.select_random(target_list,
                                      n=number,
                                      seed=seed,
                                      verbosity=verbosity)
    return target_list


# utility function to construct output string
def output_(start,
            duration,
            distance,
            targets,
            times,
            summary=False):
    def __summary__(line, vector, astro=False):
        for val in vector:
            if astro:
                line += '{: <27}'.format(val.name)
            else:
                line += '{: <27}'.format(str(val))
        line += '\n'
        return line
    writeline = 'start time {} UTC\n'.format(start)
    writeline += '  duration\t{}\n'.format(duration)
    writeline += '  slew distance\t{} [deg]\n'.format(distance)
    if summary:
        writeline += __summary__('  queue\t\t', targets, astro=True)
        writeline += __summary__('  obs times\t', times)
    else:
        writeline += '  targets\n'
        for idx, val in enumerate(targets):
            writeline += '\t{} {}\n'.format(val.name, str(times[idx]))
    print(writeline)


if __name__ == '__main__':
    # input arguments
    args = cli_()

    # mimic target input
    # Name, RA (HH:MM:SS.f), Decl (DD:MM:SS.f), duration (sec), misc
    target_list = input_(args.meertime,
                         args.catalogue,
                         number=args.number,
                         seed=args.seed,
                         verbosity=args.verbosity,
                         )
    print('\ninput target list')
    if args.verbose:
        for input_tgt in target_list:
            print(input_tgt)

    print('\ntelescope')
    # set telescope object
    meerkat = Observatory()
    if args.verbose:
        print(meerkat.telescope)

    # minimum slew distance target order
    queue = Queue(target_list,
                  observer=meerkat.telescope,
                  horizon=args.horizon,
                  obstime=args.datetime)
    print('\ntargets')
    if args.verbose:
        utils.ptable(queue.targets)

    queue_output = queue.min_slew(verbosity=args.verbosity)
    print('\noptimized')
    if args.verbose:
        utils.ptable(queue.targets[queue_output.path])

    # display optimal queue
    if len(queue_output.path) < 6:
        if args.all:
            print('\npermutations')
            for perm in queue_output.paths:
                output_(perm['start'],
                        perm['obsduration'],
                        perm['distance'],
                        queue.targets[perm['tour']]['astro_target'],
                        perm['obstimes'],
                        summary=True)
        else:
            writeline = output_(queue_output.starttime,
                                queue_output.duration,
                                queue_output.pathlen,
                                queue.targets[queue_output.path]['astro_target'],
                                queue_output.obstimes)

    print('\noutput')
    target_queue = queue.targets[queue_output.path]
    # plot_queue.elevations(target_queue,
    plot_queue.show(target_queue,
                    meerkat,
                    obstimes=queue_output.obstimes)
    plt.show()

# -fin
