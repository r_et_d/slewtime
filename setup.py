from setuptools import setup
import glob

__version__ = '0.0.1'

setup(name='slewtime',
      version=__version__,
      description='Minimize slew time',
      long_description='Minimize slew time between targets during observation',
      license='GPL',
      author='Ruby van Rooyen',
      author_email='ruby at ska.ac.za',
      classifiers=[
                   'Development Status :: 2 - Alpha',
                   'Intended Audience :: Scientists',
                   'Operating System :: OS Independent',
                   'License :: OSI Approved :: GNU General Public License (GPL)',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Physics :: Astronomy :: Python',
                  ],
      requires=['pylab', 'matplotlib', 'astropy', 'numpy', 'scipy'],
      provides=['slewtime'],
      packages=['slewtime'],
      scripts=glob.glob('scripts/*.py'),
      )
# -fin-
