# object to hold celestial information for optimization
from astropy import units as u
from astropy.time import Time
from datetime import timedelta

import numpy as np


class Astro(object):
    """Info for celestial target evaluation"""
    def __init__(self,
                 targets,
                 durations,
                 observatory,
                 horizon=15.):
        self.targets = targets
        self.durations = durations
        self.observer = observatory
        self.horizon = horizon * u.deg

    def visibility(self,
                   target,
                   start_time):
        """cannot observe node, not visible yet"""
        [visible_,
         hor_coord] = self.observer.target_is_up(start_time,
                                                 target,
                                                 horizon=self.horizon,
                                                 return_altaz=True)
        return [visible_, hor_coord]

    def risetime(self,
                 target,
                 astrotime):
        """get rise time of target for observation"""
        rise_time = self.observer.target_rise_time(astrotime,
                                                   target,
                                                   horizon=self.horizon,
                                                   which='next')
        if np.isnan(float(rise_time.unix)):
            rise_time = Time(astrotime)
        return rise_time

    def addtime(self,
                datetime_,
                dt_sec):
        """add seconds to astropy time object"""
        dt_time = Time(datetime_ + timedelta(seconds=dt_sec))
        return dt_time

    def adjusttimes(self,
                    target,
                    starttime,
                    duration):
        """observation start and end times depending
           on target visibility
        """
        # TODO: checking only start time, need to check end
        [visible_,
         hor_coord] = self.visibility(target,
                                      starttime)
        if not visible_:
            rise_time = self.risetime(target,
                                      starttime)
            delta_t = rise_time.datetime - starttime.datetime
            starttime = self.addtime(starttime.datetime,
                                     delta_t.total_seconds())

        # add observation duration time
        endtime = self.addtime(starttime.datetime,
                               duration)

        return [starttime, endtime]

    def slewtime(self,
                 slewangle):
        # add time for slew
        slew_speed = 2.  # deg/s
        slew_time = slewangle / slew_speed  # sec
        return slew_time

# -fin-
