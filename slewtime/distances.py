# various distance calculations
import numpy as np


# angular separation calculation
def separation(coord, compare, method='angular'):
    distance = []
    for nidx, next_ in enumerate(compare):
        dist_ = coord.separation(next_).degree
        distance.append(dist_)
    return np.array(distance)

# -fin-
