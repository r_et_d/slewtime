from . import utils
from astroplan import Observer
from astropy import units as u
from astropy.coordinates import EarthLocation
from astropy.coordinates import Latitude, Longitude
from astropy.time import Time

import numpy as np


class Observatory(object):
    """define the observatory in Earth coordinates
       default coordinates to center of the MeerKAT array"""

    def __init__(self,
                 name='MeerKAT',
                 lat="-30:42:39.8",
                 lon="21:26:38.0",
                 alt=1035.0,
                 horizon=20.):
        latitude = Latitude(lat, unit=u.deg)
        longitude = Longitude(lon, unit=u.deg)
        height = alt * u.m
        self.location = EarthLocation.from_geodetic(lat=latitude,
                                                    lon=longitude,
                                                    height=height)
        self.telescope = Observer(location=self.location,
                                  name=name,
                                  timezone="Africa/Johannesburg")

        self.horizon = horizon

    def _lst_range_(self,
                    midnight=None):
        """calculate the UTC and associated LST for a given date"""
        if midnight is None:
            midnight = utils.midnight()
        midnight = Time(midnight)
        delta_time = np.linspace(0, 24, 3600) * u.hour
        utc_range = Time(midnight + delta_time,
                         location=self.location)
        lst_range = utc_range.sidereal_time('apparent')
        return [utc_range, lst_range]


# -fin-
