from astropy.coordinates import AltAz
from datetime import timedelta

import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np


# plot observation order and target elevation
def elevation(ax,
              targets,
              utc_range,
              pointing_model,
              obstimes=None,
              durations=None,
              clrs=None):

    for idx, target in enumerate(targets):
        sky_coord = target.coord
        # plot targets
        clr = clrs[idx % len(clrs)]
        elevation = sky_coord.transform_to(pointing_model).alt
        ax.plot_date(utc_range.datetime,
                     elevation.value,
                     color=clr,
                     linestyle='-',
                     fmt='-',
                     linewidth=1,
                     alpha=0.2)
        if obstimes is not None:
            obstime = obstimes[idx]
            duration = durations[idx]
            starttime = np.argmin(np.abs(utc_range.datetime - obstime.datetime))
            endtime = utc_range[starttime].datetime + timedelta(seconds=duration)
            endtime = np.argmin(np.abs(utc_range.datetime - endtime))
            ax.plot_date(utc_range[starttime:endtime].datetime,
                         elevation[starttime:endtime].value,
                         color=clr,
                         linestyle='-',
                         fmt='-',
                         linewidth=1)
            ax.axvspan(utc_range[starttime].datetime,
                       utc_range[endtime].datetime,
                       alpha=0.1,
                       color=clr)
    ax.tick_params(axis='x', labelsize=8)
    ax.tick_params(axis='y', labelsize=8)
    myFmt = mdates.DateFormatter("%H:%M")
    ax.xaxis.set_major_formatter(myFmt)
    ax.set_ylim(0, 90)
    ax.fill_between(utc_range.datetime, 0, 15., color='0.9', zorder=0)
    ax.set_ylabel('Elevation [deg]', fontsize=10)


def location(ax, targets, clrs=None):
    for idx, target in enumerate(targets):
        sky_coord = target.coord
        # plot targets
        clr = clrs[idx % len(clrs)]
        ax.scatter(sky_coord.ra.degree,
                   sky_coord.dec.degree,
                   color=clr)
        ax.annotate('[{}] {}'.format(idx, target.name),
                    xy=(sky_coord.ra.degree, sky_coord.dec.degree),
                    color='k', size=8)
    ax.tick_params(axis='x', labelsize=8)
    ax.tick_params(axis='y', labelsize=8)
    ax.set_xlabel('RA [deg]', fontsize=10)
    ax.set_ylabel('Dec [deg]', fontsize=10)


# plot summary graph showing observation sequence
def show(targets,
         telescope,
         obstimes=None,
         ):

    # elevation with respect to MeerKAT
    # use starttime from observation list for uct range
    midnight = obstimes[0].datetime - timedelta(hours=1.)
    [utc_range, lst_range] = telescope._lst_range_(midnight=midnight)
    pointing_model = AltAz(obstime=utc_range,
                           location=telescope.location)

    # display the results
    clrs = ['b', 'g', 'r', 'c', 'm']
    fig = plt.figure(figsize=(15, 6),
                     facecolor='white',
                     constrained_layout=True)
    gs = gridspec.GridSpec(ncols=10,
                           nrows=9,
                           figure=fig)
    ax0 = fig.add_subplot(gs[:4, :])
    ax1 = fig.add_subplot(gs[4:, :3])
    ax2 = fig.add_subplot(gs[5:, 4:])
    ax2.yaxis.set_label_position("right")
    ax2.yaxis.tick_right()

    elevation(ax0,
              targets['astro_target'],
              utc_range,
              pointing_model,
              obstimes=obstimes,
              durations=targets['duration'],
              clrs=clrs)
    ax0.set_xlabel("Time (UTC) starting on {}".format(
                   utc_range[0].datetime.strftime("%Y-%m-%d")),
                   fontsize=10)
    location(ax1,
             targets['astro_target'],
             clrs=clrs)
    elevation(ax2,
              targets['astro_target'],
              utc_range,
              pointing_model,
              obstimes=obstimes,
              durations=targets['duration'],
              clrs=clrs)
    if obstimes is not None:
        ax2.set_xlim([obstimes[0].datetime - timedelta(minutes=30.),
                      obstimes[-1].datetime + timedelta(minutes=30.)])

# -fin-
