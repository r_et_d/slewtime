from . import utils
from .traveling_salesman import TSP
from astropy.time import Time
from datetime import datetime


class Queue(object):
    def __init__(self,
                 targetlist,
                 observer,
                 horizon=15.,
                 obstime=None):
        # astropy coordinates for easy optimisation
        self.observer = observer
        # set date for observation planning
        if obstime is None:
            obstime = utils.midnight()
        else:
            obstime = datetime.strptime(obstime,
                                        '%Y-%m-%d %H:%M:%S')
        self.datetime = Time(obstime)
        self.horizon = horizon  # degree

        # build target list for optimization
        self.targets = utils.build_recarray(targetlist,
                                            observer,
                                            obstime,
                                            horizon=horizon)

    def sort_targets(self,
                     sortorder=None,
                     reverse=False):
        """sort first by x then by y coordinates"""
        self.targets.sort(order=sortorder)
        # reverse order, you want to observe setting targets first
        if reverse:
            self.targets = self.targets[::-1]

    def min_slew(self,
                 verbosity=0):
        """minimum angular slew time between targets
           optionally
           trimmed to show only targets visible at observations time
           and weighted to prefer observing targets as high as possible"""

        # sort by RA and DECL first
        # pre-ordering will make path search a little quicker
        self.sort_targets(sortorder=['rise_time'],
                          reverse=True)
        if verbosity > 0:
            print('\nordered targets')
            utils.ptable(self.targets)

        # apply travelling salesman algo to find minimum distance
        tsp = TSP(self.targets['astro_target'],
                  durations=self.targets['duration'],
                  observatory=self.observer,
                  horizon=self.horizon,
                  )
        # greedy construction nearest neighbour
        tsp.nneigh(start_time=self.datetime,
                   verbosity=verbosity)

        # select the best solution based on time or distance
        tsp.optimise(verbosity=verbosity)

        # calculate total slew time (path length)
        if verbosity > 1:
            writeline = "\noptimized path {}\n".format(tsp.path)
            writeline += "  starting @ {}\n".format(tsp.starttime)
            writeline += "  total time: {}\n".format(tsp.duration)
            writeline += "  total slew: {} deg".format(tsp.pathlen)
            print(writeline)

        return tsp

# -fin-
