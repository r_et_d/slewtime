# options for calculating the optimal traveling salesman path
from . import utils
from .astro_target import Astro
from .distances import separation
from astropy.time import Time
from datetime import datetime
from numpy import recarray

import numpy as np


# --- Utility astro function for TSP ---
def astro_eval(index,
               obs_info,
               start_time,
               startvector=None,
               verbosity=0):
    if startvector is None:
        startvector = []
    astro_target = obs_info.targets[index]
    obs_duration = obs_info.durations[index]
    if verbosity > 1:
        print('Tour starts: {0.iso}, JD: {0.jd}'.format(
            start_time))

    # cannot start with index node, if not visible yet
    # wait for it to rise
    [start_time,
     dt_time] = obs_info.adjusttimes(astro_target,
                                     start_time,
                                     obs_duration)
    startvector.append(start_time)

    if verbosity > 1:
        print('Observe {} start {} for duration {} sec'.format(
            obs_info.targets[index].name,
            start_time.datetime.strftime('%Y-%m-%d %H:%M:%S'),
            obs_info.durations[index]))
    if verbosity > 2:
        print('after obs: {0.iso}, JD: {0.jd}'.format(
            dt_time))
    return [startvector, dt_time]
# --- Utility astro function for TSP ---


# --- Utility component function for TSP ---
# separation distance matrix
def distance_matrix(nodes,
                    method):  # angular or linear distance
    n = nodes.size
    dist = np.zeros((n, n), dtype=float)
    for idx, value in enumerate(nodes[:-1]):
        dist[idx, idx + 1:] = separation(value,
                                         nodes[idx + 1:],
                                         method=method)
    dist += dist.T
    return dist


# calculate tour distance
def distance(tour, distM):
    node_idx = np.arange(0, len(tour) - 1)
    next_node_idx = node_idx + 1
    # add length from this node to next
    path = np.sum(distM[tour[node_idx], tour[next_node_idx]])
    return path


# Return tour starting from city 'i', using the Nearest Neighbor
def nearest_neighbor(cnt,
                     seed,
                     distM,
                     start_time=None,
                     obs_info=None,
                     verbosity=0):

    if start_time is None:
        obs_info = None

    unvisited = list(range(cnt))
    unvisited.remove(seed)
    last = seed
    tour = [seed]

    if obs_info is not None:
        [startvector,
         dt_time] = astro_eval(seed,
                               obs_info,
                               start_time,
                               verbosity=verbosity)
        start_time = startvector[0]

    # find nearest visible target
    # TODO: divide and conquer implementation
    while len(unvisited) > 0:
        dist_ = distM[np.array(len(unvisited) * [last]),
                      unvisited]
        idx_ = np.argmin(dist_)
        nearest_ = unvisited[idx_]

        if verbosity > 1:
            print('next visit options',
                  [np.array(len(unvisited) * [last]),
                   unvisited])
            print('distance for each', dist_)

        # if astro target, check visibility
        # nearest target will prob also be closest to rising
        if obs_info is not None:
            # add time for slew
            slew_angle = dist_[idx_]
            slew_time = obs_info.slewtime(slew_angle)
            if verbosity > 1:
                print('slew {} deg to nearest node {}'.format(
                    slew_angle, nearest_))
                print('estimated time slewing {} sec'.format(
                    slew_time))
            dt_time = obs_info.addtime(dt_time.datetime,
                                       slew_time)
            if verbosity > 2:
                print('after slew: {0.iso}, JD: {0.jd}'.format(
                    dt_time))

            [startvector,
             dt_time] = astro_eval(nearest_,
                                   obs_info,
                                   dt_time,
                                   startvector=startvector,
                                   verbosity=verbosity)

        tour.append(nearest_)
        unvisited.remove(nearest_)
        last = nearest_
        if verbosity > 1:
            print()
            print('nearest node', nearest_, 'of', unvisited)
            print('new tour', tour)
            print('remaining nodes', unvisited)
            print()

    # total observation time for tour
    obs_time = dt_time.datetime - start_time.datetime
    if verbosity > 1:
        print('Tour ends: {0.iso}, JD: {0.jd}'.format(
            dt_time))
        print('Total observation time: {}'.format(
            obs_time))

    return [obs_time, np.array(tour), np.array(startvector)]
# --- Utility component function for TSP ---


class TSP(object):
    """Object for solving the traveling salesman problem"""
    def __init__(self,
                 targets,
                 observatory=None,
                 durations=[0.],  # time on target
                 horizon=15.,  # min pointing angle
                 distance_measure='angular',  # angular separation
                 ):
        self.targets = targets
        # nodes = list of SkyCoord objects
        self.nodes = np.array([tgt.coord for tgt in targets])
        self.cnt = self.nodes.size
        self.distM = distance_matrix(self.nodes,
                                     method=distance_measure)

        # add info for celestial target evaluation
        self.obs_info = None
        if observatory is not None:
            if len(durations) < len(targets):
                durations = np.repeat(durations[0],
                                      len(targets))
            self.obs_info = Astro(targets,
                                  durations,
                                  observatory,
                                  horizon=horizon)
        desc = {'names': ('seed',
                          'tour',
                          'obsduration',
                          'distance',
                          'start',
                          'obstimes',
                          ),
                'formats': (int,
                            list,
                            Time,
                            float,
                            datetime,
                            list,
                            ),
                }
        self.paths = recarray((0,), dtype=desc)
        self.path = None
        self.pathlen = np.inf  # degrees slew
        self.duration = None
        self.starttime = None
        self.obstimes = None

    # greedy find of best path using nearest neighbour
    def nneigh(self,
               start_time,
               verbosity=0):
        if verbosity > 1:
            print("greedy nearest neighbor --> local search:")

        for seed in range(self.cnt):
            if verbosity > 1:
                print('Start @ {} seeded @ {}'.format(
                    start_time.datetime, seed))
            [obstime_,
             tour_,
             starts_] = nearest_neighbor(self.cnt,
                                         seed,
                                         self.distM,
                                         start_time=start_time,
                                         obs_info=self.obs_info,
                                         verbosity=verbosity)
            # calculate tour length
            dist_ = distance(tour_, self.distM)
            if verbosity > 2:
                writeline = "{}: {}\n".format(seed, tour_)
                writeline += "  starting @ {}\n".format(starts_[0])
                writeline += "  total time: {}\n".format(obstime_)
                writeline += "  total slew: {} deg".format(dist_)
                print(writeline)
            self.paths.resize(self.paths.size + 1)
            self.paths[-1] = (seed,
                              tour_,
                              obstime_,
                              dist_,
                              starts_[0].datetime,
                              starts_)

    def optimise(self,
                 verbosity=0.):
        # visit as many places as possible but travel as short as possible
        sort_order = ['obsduration', 'distance', 'start']

        self.paths.sort(order=sort_order)
        if verbosity > 1:
            utils.ptable(self.paths)

        self.path = self.paths[0]['tour']
        self.pathlen = self.paths[0]['distance']
        self.duration = self.paths[0]['obsduration']
        self.starttime = self.paths[0]['start']
        self.obstimes = self.paths[0]['obstimes']

        if verbosity > 2:
            writeline = "\n{}\n".format(self.path)
            writeline += "  starting @ {}\n".format(self.starttime)
            writeline += "  total time: {}\n".format(self.duration)
            writeline += "  total slew: {} deg".format(self.pathlen)
            print(writeline)
            print(self.obstimes)

# -fin-
