from astroplan import FixedTarget
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time
from numpy import recarray

import datetime
import numpy as np
import random
import sys


def ptable(recarray,
           columns=None,
           tidy=False):
    """pretty print recarray as table
       optional sort by list of columns
    """
    from prettytable import PrettyTable
    x = PrettyTable(recarray.dtype.names)

    if tidy:
        # default sort by name
        recarray.sort(order=recarray.dtype.names)
    if columns is not None:
        # sort by columns as specified
        recarray.sort(order=columns)

    for row in recarray:
        x.add_row(row)
    print(x)


def select_random(list_,
                  n=10,
                  seed=None,
                  verbosity=0):
    """Random selection of n targets from list
       List items assumed format:
           Name, RA (HH:MM:SS.f), Decl (DD:MM:SS.f), duration (sec), misc
       Random selection of n targets, which may be seeded
    """
    if seed is not None:
        random.seed(seed)
    if verbosity > 1:
        # display debug output
        func_name = sys._getframe().f_code.co_name
        print('\nfunction {}:'.format(func_name))
        # for debugging set seed to zero
        # pseudo-random with seed will always give "selection"
        if seed is None:
            seed = 0
        print('apply seed = {}'.format(seed))
        random.seed(seed)
    # inline randomization of targets in list
    random.shuffle(list_)
    # extract number of targets
    nr_items = min((len(list_), n))
    return list_[:nr_items]


def build_recarray(target_list,
                   observer,
                   obsdate,
                   horizon=20.,
                   verbosity=0):
    """build numpy recarray with name and coordinates using astropy
       input: target_list : list of strings to unpack to get target info
              observer : use telescope object as observer
              obsdate : date to stat list observation
              verbosity [optional] : display target info as it unpacks
    """
    desc = {'names': ('astro_target',
                      'duration',
                      'rise_time',
                      ),
            'formats': (FixedTarget,
                        np.float,
                        Time,
                        ),
            }
    targets = recarray((0,), dtype=desc)

    # unpack targets into astropy targets
    for target in target_list:
        target_data = target.split(',')
        # assume input format: name, tags, ra, dec, flux model
        [name,
         ra, decl,
         duration] = map(target_data.__getitem__, (0, 1, 2, 3))
        # create astropy targets for optimisation
        # TODO: you need to sort out the unit defs to be more general
        coord = SkyCoord(ra.strip(), decl.strip(),
                         unit=(u.hourangle, u.deg),
                         frame='icrs')
        astro_target = FixedTarget(coord=coord,
                                   name=name)
        horizon += 0.1  # small step increase for rounding
        rise_time = observer.target_rise_time(Time(obsdate),
                                              astro_target,
                                              horizon=horizon * u.deg,
                                              which='next')
        if np.isnan(float(rise_time.unix)):
            rise_time = Time(obsdate)

        if verbosity > 1:
            # display debug output
            func_name = sys._getframe().f_code.co_name
            print('\nfunction {}'.format(func_name))
            print('Input coordinate [{} {}]'.format(ra, decl))
            print(coord)
            print('Astropy coordinate {}'.format(
                astro_target.coord.to_string('hmsdms',
                                             alwayssign=True)))
            # replace with a given string
            print('rise_time')
            print('ISO: {0.iso}, JD: {0.jd}'.format(rise_time))

        # build array of targets
        targets.resize(targets.size + 1)
        targets[-1] = (astro_target,
                       duration,
                       rise_time,
                       )
    return targets


def midnight():
    date = datetime.date.today()
    midnight = datetime.datetime.combine(date,
                                         datetime.time.min)
    return midnight

# -fin-
